AR = 'ar qc'
LIBTOOL = 'libtool'

CC = 'cc'
CFLAGS += '-DGL_SILENCE_DEPRECATION'
CFLAGS += '-g'

LD = 'cc'
LDFLAGS += '-isysroot `xcrun --show-sdk-path`'
LDFLAGS += '-g'

LIBRARIES += '-framework Cocoa'
LIBRARIES += '-framework OpenGL'
LIBRARIES += '-framework CoreFoundation'