tup.creategitignore()
tup.include( tup.getconfig('TUP_PLATFORM') .. '.lua' )


function cc( input )
    tup.foreach_rule( input, '^c CC %f^' .. tostring(CC) .. ' ' .. tostring(CFLAGS) .. ' -c %f -o %o', '%B.o' )
end

function libtool( input, output )
    tup.rule( input, tostring(LIBTOOL) .. ' -o %o %f', output )
end

function ar( input, output )
    tup.rule( input, tostring(AR) .. ' %o %f', output )
end

function exe( input, output )
    tup.rule( input, '^c LD %f^' .. tostring(LD) .. ' ' .. tostring(LDFLAGS) .. ' %f -o %o ' .. tostring(LIBRARIES), output )
end